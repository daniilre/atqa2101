
package com.mycompany.jsonparsermavenexample;


import myobjects.BeanA;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;



public class JSONParserMavenExample {


    public static void main(String[] args) throws UnsupportedOperationException, IOException {
        System.out.println("Hello from JSONParserMavenExample");
        String json = "{bool:true,integer:1,string:\"json\"}"; 
        //термин обычный класс на java = bean class
        JSONObject jsonObject = JSONObject.fromObject( json );  
        BeanA bean = (BeanA) JSONObject.toBean( jsonObject, BeanA.class );
        System.out.println("JSON serialize test 1");
        System.out.println(bean);
                
        //
        String result = "";
      String urladr = "https://0aff146f-63f6-4ea5-b05b-0857922e27cc.mock.pstmn.io";
      
      result = getResponseToJson(urladr, result);
        
      System.out.println("input data:");
      System.out.println(result);
        
        //
    }
    
    //
    //добавляем функцию, которая содержит код для работы с запросами к серверу
    public static String getResponseToJson(String urladr, String result) throws UnsupportedOperationException, IOException {
        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpGet httpget = new HttpGet(urladr);
        System.out.println("Request Type: "+httpget.getMethod());
        //Executing the Get request
        HttpResponse httpresponse = httpclient.execute(httpget);
        Scanner sc = new Scanner(httpresponse.getEntity().getContent());
        //Printing the status line
        System.out.println(httpresponse.getStatusLine());
        while(sc.hasNext()) {
            String line = sc.nextLine();
            System.out.println(line);
            result+=line;
        }
        return result;
    }
    //
}
