
package taxcalculator;


public class MyDate {

    public MyDate(int day, String month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }

    @Override
    public String toString() {
        return "MyDate{" + "day=" + day + ", month=" + month + ", year=" + year + '}';
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        if (day >= 1 & day <= 31)
        {
            this.day = day;
        }
        else
        {
            this.day = 1;
        }
    }
    private int day;
    private String month;
    private int year;
    
    // конструктор = правило создания 
    // объекта в начальном безопасном остоянни
    // имя конструктора = имя функции = имя класс
    
}
