
package cycleforexample;


public class CycleForExample {

    
    public static void main(String[] args) {
        
        System.out.println("Вывод на экран 1");
        for(int i = 0 ; i <= 3 ; i++)
        {
            System.out.print(i+" ");
        }
        System.out.println();
        
        System.out.println("Вывод на экран 2");
        for (int o = 3 ; o >= 0 ; o--) // но тут он уже не знает, "i" из предыдущей for и можно опять назвать "i" здесь вместо мего "o"
        {
            System.out.print(o);
            System.out.print(" ");
        }
        System.out.println();
        
    }
    
}
