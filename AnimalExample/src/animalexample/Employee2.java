
package animalexample;


public class Employee2 extends Person{

    @Override
    public String toString() {
        return "Employee2: " +
                "position = " + position +
                ", personal number = " + personal_number +
                ", salary = " + salary + ", "
                + super.toString();
    }
    
    private String position;
    private int personal_number;
    private int salary;
    
    Employee2 (String fname, String lname, int sex, int age, String email, String phone, String position, int pnumber, int salary)
    {
        super (fname, lname, sex, age, email, phone);
        this.position = position;
        this.personal_number = pnumber;
        this.salary = salary;
    }
    
}
