
package animalexample;


public class Employee extends Man{


    @Override
    public String toString() {
        return "Employee: " +
                "position = " + position +
                ", personal number = " + personal_number +
                ", salary = " + salary + ", " 
                + super.toString();
    }
    
    private String position;
    private int personal_number;
    private int salary;
    
    Employee(String fname, String lname, int sex, int age, String position, int pnumber, int salary)
    {
        super (fname, lname, sex, age);
        this.position = position;
        this.personal_number = pnumber;
        this.salary = salary;
    }
    
}
