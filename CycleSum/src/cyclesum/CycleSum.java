
package cyclesum;


public class CycleSum {

   
    public static void main(String[] args) {
        
        // 1 циклы и числа: (суммы рядов)
        // вывести сумму первых десяти целых чисел
        // сумму четырех/ нечетных чисел
        // вывести сумму целого числа
        
        //1.1
        int sum = 0;
        for (int i = 1; i <=10; i++)
        {
            System.out.println(i);
            sum = i + sum; // вычисление по x = x + 1
        }
        System.out.println("Сумма = "+sum);
        
        //1.2
        sum = 0; // инициализируем существующую переменную
        System.out.println("четные: ");
        for (int i=1; i<=10; i = i + 1)
        {
          int m = i % 2;
          if ( m == 0)
          {
              //четное
              sum  = sum + i;
              System.out.println(i);
          }
//          else // m != 0 не равено нулю
//          {
              //нечетное
//              
//          }
        }
        System.out.println("Сумма четных = "+sum);
        System.out.println("нечетные:");
        sum = 0;
        for (int i = 1; i <= 10; i = i + 2)
        {
            System.out.println(i);
            sum = sum + i;
        }
        System.out.println("Сумма нечетных = "+sum);
        
        //1.3
        int x = 7354;
        System.out.println("Число = "+x);
        int counter = 0;
        int sumDigits = 0;
        while (x != 0)
        {
            sumDigits = sumDigits + x%10;
            x = x / 10;
            counter = counter + 1;            
        }
        System.out.println("Количество цифр в нем "+counter);
        System.out.println("Сумма цифр = "+sumDigits);
        
        //2 циклы и строки
        
        
    }
    
}
