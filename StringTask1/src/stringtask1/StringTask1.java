
package stringtask1;


public class StringTask1 {

    
    public static void main(String[] args) {
        
        String str = "world! w a w";
//        int a = str.length();
        String str2 = "w";
//        int b = str2.length();
        
        boolean state2 = StringTask1.isCharPresent(str, str2);
        
        if (state2 == true)
        {
            System.out.println("Letter " + str2 + " is present in the text.");
        }
        else
        {
            System.out.println("Letter " + str2 + " is NOT present in the text.");
        }
        
        System.out.println("Letter "+ str2 + " is found " + StringTask1.calcChar(str, str2) + " times");
        
//        for (int i = 0; i < str.length(); i++)
//        {
//            if (str.charAt(i) != 'w' )
//            {
//            }
//        }
    }
    
    public static boolean isCharPresent(String str, String str2)
    {
        boolean state = false;
//        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < str.length(); i++)
        {
         char c = str.charAt(i);
                  
         if (str2.charAt(0) == c)
         {
             state = true;
         break;
         }
//         else
//         {
//             state = false;
//         }
        }
        
        return state;
    }
    
    public static int calcChar (String str, String str2)
    {
        int sum = 0;
        for (int i = 0; i < str.length(); i++)
        {
            char c = str.charAt(i);
            
            if (str2.charAt(0) == c)
            {
                sum++;            
            }
            
        }
        return sum;
    
    }
    
}
