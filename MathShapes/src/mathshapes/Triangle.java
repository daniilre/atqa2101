
package mathshapes;


public class Triangle extends Rectangle implements IMathShape{

    private int c;
    public Triangle (int a, int b, int c)
    {
        super(a,b);
        this.setC(c);
    }
    
    public int getC() {
        return c;
    }

    public void setC(int c) {
        this.c = c;
    }
    //
    @Override
    public double caclArea()
    {
        //формула Герона 
        double a = this.getA();
        double b = this.getB();
        double c = this.getC();
        double p = this.calcPerimetr() / 2;
        double s = Math.sqrt(p * (p-a)*(p-b)*(p-c));
        return s;
//        return this.getA()* this.getB();
    }
    @Override
    public double calcPerimetr()
    {
        return this.getA() + this.getB() + this.getC();
    }
    @Override
    public String toString() {
        return super.toString() + " c = " + this.getC();
    }
    //
    @Override
    public String getMyClassName() {
        return "Triangle";
    }
    //
    
    
}
