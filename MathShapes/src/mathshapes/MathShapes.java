
package mathshapes;

import java.util.ArrayList;
import java.util.List;


public class MathShapes {

    
    public static void main(String[] args) {
        //Создать понятия квадрат, прямоуголник, треугольник
        // связать эти понятия наследованием
        
        // в будущем может появиться круг (и точка?)
        // нужно уметь вычислять площадь и периметр фигуры
        
        //квадрат a = 3
        //S = 9 площадь ; P = 12 периметр
        //
        Square sq = new Square(3);
//        double sqArea = sq.caclArea();
//        double sqPerimetr = sq.calcPerimetr();
//        System.out.println("Square: S = " + sqArea + " P = " + sqPerimetr);
        
        //прямоугольник а = 3, b = 4
        //S = 12; P = 14
        Rectangle rec = new Rectangle(3, 4);
//        double recArea = rec.caclArea();
//        double recPerimetr = rec.calcPerimetr();
//        System.out.println("Rectangle "+ rec);
//        System.out.println("Rectangle: S = " + recArea + " P = " + recPerimetr);
        
        //треугольник a = 3, b = 4, b =5 
        //S = 6, P = 12
        Triangle trg = new Triangle(3, 4, 5);
//        double p = trg.calcPerimetr();
//        double s = trg.caclArea();
//        System.out.println("Triangle: S = " + s + " P = " + p);
        
        
        List<IMathShape> shapes = new ArrayList<>();
        shapes.add(sq);
        shapes.add(rec);
        shapes.add(trg);
//        list.charAt(1); // Обратиться ко второй букве
        for (int i = 0; i < shapes.size(); i++)
        {
            double P = shapes.get(i).calcPerimetr();
            double S = shapes.get(i).caclArea();
            System.out.println(shapes.get(i).getMyClassName());
            System.out.println(shapes.get(i).toString());
            System.out.println("S = " + S + " P = " + P);
        
        }
        
//        IMathShape shape1 = new Square(3);
//        IMathShape shape2 = new Rectangle(3, 4);
//        IMathShape shape3 = new Triangle (3,4,5);
//        
//        IMathShape shape = shape3;
        
//        shapes.caclArea();
//        shapes.calcPerimetr();
        
        
    }
    
}
