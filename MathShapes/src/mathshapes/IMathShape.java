
package mathshapes;

// тут чисто методы
public interface IMathShape { 
    
    public double caclArea();
    public double calcPerimetr();
    public String getMyClassName();
    
}
