
package txtfiletester;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.FileReader;

import java.io.PrintWriter;// вывод данных (куда угодно)
//в таком же формате как и System.out.println();

import java.io.IOException;// механизм обработки любой ошибки
import java.io.InputStreamReader;


public class TxtFileTester {


    public static void main(String[] args) {
        System.out.println("Введите строки для записи в файл");
        System.out.println("остановка записи - stop");
        PrintWriter output = null;
        try {
            
            //создаем файл и пишем в него данные
            BufferedReader input = new BufferedReader(
                    new  InputStreamReader(System.in));
            output = new PrintWriter(
                    new FileWriter("data.txt"));
            while(true)
            {
                //считываем воод с клавиатуры и записываем, то что
                //прочли в файл
                String strtmp = input.readLine();
                if (strtmp.equals("stop"))//если ввели слово stop прекратили
                    break;
                output.println(strtmp);
            }
            output.close();
        } catch (IOException ex) {
            System.out.println("Не могу создать файл. Нет прав на запись.");
        }
        finally{
           output.close(); 
        }
        
        //тение файла
        
        
    }
    
}
