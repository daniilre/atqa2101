
package junitexample1;


public class TestTask {
    //
    public static boolean assertExpression(boolean result, boolean expected)
    {
        if (result == expected)
        {
            System.out.println("log: assert expression is true");
            return true;
        }
        else
        {
            System.out.println("log: assert expression is false");
            return false;
        }
    }
    //
    
    //1 ChildrentestTask
    private Children chi;
    public void doSetUpClass()
    {
       System.out.println("0. Один раз"); 
       chi = new Children("Ivan Pertov");
        
    }
    public void doSetUp()
    {
        System.out.println("1. Каждый раз перед методом doTest");
    }
    public void doTearDownClass()
    {
        System.out.println("Один раз в самом конце");
    }
    public void doTearDown()
    {
        System.out.println("2. Каждый раз. После");
        //здесь закрытие соединения с базой данных
        //закрытие файлов
        //удаление соединения с сайтом
    }
    //первый тест является ли класс наследником
    //общие принципы назвать имя черещ нижнее подчеркиание _
    //слово test - описание теста - параметры - ожидаемое значение - результат
    //шаблон test_descriptiontest_params_expected_result
    //test_isParentCorrect_Children_Parent_expected_true_pass
    public boolean test_haveCorrectParent_true_pass()
    {
        //в JUnit метод возвращает void
        //но заканчивается командой assert
        boolean result = false;
        //
        Children chi = new Children("Ivan Pertov");
        if (chi instanceof Parent )
        {
            //System.out.println("Класс Children наследует Parent");
            //System.out.println(chi.getClass() + " in a " + Parent.class);
            result = true;
        }
        //
        return result;
    }
    //второй тест работает ли метод sayHello
    //нужно его изменить имя
    //и написать проверку
    public void test_canCallSayHello_true_pass()
    {
        boolean result = false;
        //
        result = this.chi.sayHello().equalsIgnoreCase("Hello, my name is Ivan Pertov");
        //
        boolean expected = true;
        assertExpression(result, expected);
        
    }
    //
    public void test_IsTaxMinSalaryCorrect_true_pass()
    {
        boolean result = false;
        double minSalary = 6000;
        Tax tax = TaxCalc.calcTax(minSalary);
        if(
               tax.pension == minSalary * 0.22
            && tax.pn == minSalary * 0.18
            && tax.prof == minSalary * 0.04
                )
        {
            result = true;
        }
        else
        {
            result = false;
        }
        
        boolean expected = true;
        TestTask.assertExpression(result, expected);
        
    }
    
    
}
