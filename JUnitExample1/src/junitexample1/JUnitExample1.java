
package junitexample1;


public class JUnitExample1 {


    public static void main(String[] args) {
        
        //тест иерархии extends
        //и 
        //в итоге увижу эталон теста
        //потом логику повторю 
        
//        Children chi = new Children("Ivan Pertor");
//        if (chi instanceof Parent )
//        {
//            System.out.println("Класс Children наследует Parent");
//            System.out.println(chi.getClass() + " in a " + Parent.class);
//        }
        //использует методы рефлексии 
        //программа использует класс тестов 
        //...
        //...
        TestTask test1 = new TestTask();
        test1.doSetUpClass();
        test1.doSetUp();
        test1.test_haveCorrectParent_true_pass();
        test1.doTearDown();
        test1.doSetUp();
        test1.test_canCallSayHello_true_pass();
        test1.doTearDown();
        test1.doTearDownClass();
        test1.doSetUp();
        test1.test_IsTaxMinSalaryCorrect_true_pass();
        test1.doTearDown();
        test1.doTearDownClass();
        
        
    }
    
}
