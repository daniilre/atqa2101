
package sqrtcalctest;

import java.util.Scanner;

public class SqrtCalcTest {

    
    public static void main(String[] args) {
        double x = 0;
        Scanner myInput = new Scanner ( System.in );
        do
        {
           System.out.println("Введите целое число x > 0");
           x = myInput.nextDouble();
        } while (x < 0 );
        //1 Формат данных: вместо числа слово
        // вместо разделителя запятой точка
        // Проблема формата - или перевод к строкам
        // или обработка Excpetion (исключений)
        //2 логика данныъ. Ввод отрицательный.
        // условный инструкции (if) и циклы (while, do while)
        if (x < 0)
        {
            System.out.print( "Введено отрицательное. " );
            return;// завершаем работы функции с результатом
        }
        
        double y = Math.sqrt(x); // если x > 0 
        System.out.println ("Квадратный корень из "+x+" равен = " + y);
        
        
        
//        System.out.println("Введите целое число x > 0");
//        double x = 0;
//        Scanner myInput = new Scanner ( System.in );
//        x = myInput.nextInt();
////        x = myInput.nextDouble();
//        String strX = myInput.nextLine(); // ввожу символы
//        double y = Math.sqrt(x);
//        System.out.print("Квадратный корень из ");
//        System.out.print(x);
//        System.out.print(" равен ");
//        System.out.println(y);
//        System.out.println("Квадратный корень из "+x+" равен " + y);
    }
    
}
