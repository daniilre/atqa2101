
package com.mycompany.mavenjdbctest;

//import java.util.logging.Level;
//import java.util.logging.Logger;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class mavenjdbctest {


    public static void main(String[] args) {
        try {
            System.out.println("Hello from Maven Project!");
            //0. +Подключаю драйвер
            //1. Регистрирую класс драйвера в проекте
            Class.forName("org.sqlite.JDBC");
            //2. Создаю и настраиваю соединение с базой данных
            Connection conn = null;
            conn = DriverManager.
            getConnection("jdbc:sqlite:C:\\MyData\\test.db");
            //3. Настраиваю команду языка SQL
            Statement stm = conn.createStatement();
            String text = "SELECT NAME, POSITIONS, SALARY FROM EMPLOYEES"; // надо правильно передать запрос
            //4. Выпоолняю команду языка SQL
            ResultSet rs = stm.executeQuery(text);
            //5. обрабатываю результаты в цикле
            while (rs.next())
             {
                float id = rs.getFloat("SALARY"); // правильно передать название полей
                String str1 = rs.getString("NAME");
                String str2 = rs.getString("POSITIONS");
                System.out.println("имя " + str1);
                System.out.println("должность " + str2);
                System.out.println("заработная плата "+id);
             }
            conn.close();
            //
        } catch (ClassNotFoundException ex) {
            System.out.println("не тот файл драйвера используем");
//            Logger.getLogger(mavenjdbctest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            System.out.println("Не нейден файл базы данных");
//            Logger.getLogger(mavenjdbctest.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
}
