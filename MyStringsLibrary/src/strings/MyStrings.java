
package strings;


public class MyStrings {
    
    public static String getEvenSymbols(String str)
    {
        int n = str.length();//внутри может быть цикл
//        String res = "";
        StringBuilder sb = new StringBuilder(); // это для работы с большими строками
        for (int i = 0 ; i < n; i++)
        {
            int m = (i + 1) % 2;
            if (m == 1)
            {
                // res = res + str.charAt(i); // накапливает буквы которые стоят на нечетных местах
                sb.append(str.charAt(i)); // тут мы 
                // если для + тип string
                // то он назначается конкатенацией
            }
        }
        return sb.toString();
    }
    
    public static boolean isPalyndrom(String string)
    {
        String str = string;
        boolean state = true;
        //Дз 1
        str = str.toLowerCase(); // приводим все к нижнему регистру
        str = str.trim(); // Обрезаем пробел справа и слева от слова Казак
        //str = str.replace(' ', '_'); // заменяем что хотим убрать (первые кавычки) на что заменить (вторые кавычки) Тут пробел заменили на подчеркиваение
        String tmp = "";
        for (int i = 0; i < str.length(); i++)
        {
            if (str.charAt(i) != ' ' )
            {
                tmp = tmp + str.charAt(i);//замена на StringBuilder
            }
        }
        str = tmp;
        //
        int n = str.length();
        StringBuilder sb = new StringBuilder(str);
        sb.reverse();
        String s2 = sb.toString();
        if (str.equals(s2))
        {
            //падиндром
            state = true;
        }
        else
        {
            //не палиндром
            state = false;
        }
//        for (int i = 0; i < str.length() / 2; i++)
//        {
//            if (str.charAt(i) != str.charAt( n - 1 - i))
//            {
//                state = false;
//                break;
//            }
//        }
        //
        return state;
    
    }
            
}
