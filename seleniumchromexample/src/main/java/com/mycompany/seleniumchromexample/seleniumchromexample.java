
package com.mycompany.seleniumchromexample;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.*;
import org.junit.Test;

public class seleniumchromexample {

    //задачи: 
    //1) повтор примера с сайтом
    //2) получание элемента со своей страницы (со своей страницы - ... )
    //
    public static void main(String[] args) throws InterruptedException {
        
        System.out.println("Hello from seleniumchromexample");
        //надо указать местоположение драйвера и путь к нему
        // shadow - default получается, поэтому мы не видим
        System.setProperty("webdriver.chrome.driver", "D://chromedriver_win32/chromedriver.exe");           
        WebDriver driver = new ChromeDriver(); 
        
        //создать конкретній драйвер
        driver.get("https://www.google.com/");

        Thread.sleep(5000);  // Let the user actually see something!     

        //WebElement searchBox = driver.findElement(By.name("q"));

        //searchBox.sendKeys("ChromeDriver");     

        //searchBox.submit();    

        Thread.sleep(5000);  // Let the user actually see something!     

        driver.quit();  
    }
    
}
