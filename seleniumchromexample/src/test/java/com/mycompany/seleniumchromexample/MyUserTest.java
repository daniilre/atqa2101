/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.seleniumchromexample;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author EasternPeak
 */
public class MyUserTest {
    
    public MyUserTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

//    @Test
    @org.junit.Test
    public void testCalcSum() {
        System.out.println("calcSum");
        double a = 1.0;
        double b = 2.0;
        MyUser instance = new MyUser();
        double expResult = 3.0;
        double result = instance.calcSum(a, b);
        assertTrue(result == expResult);
    }
    
}
