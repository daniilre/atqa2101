
package stringexample3;

import strings.MyStrings;


public class StringExample3 {

    
    public static void main(String[] args) {
        
        // вывести буквы, которые стоят на нечетных местах
                    //0123456789
        String str = "Аргентина манит негра";
                    //123456789
                    //А г н
        
        
                    
          //Та же функция, что и перенесли в класс  MyStrings.java          
//        int n = str.length();//внутри может быть цикл
//        String res = "";
//        for (int i = 0 ; i < n; i++)
//        {
//            int m = (i + 1) % 2;
//            if (m == 1)
//            {
//                res = res + str.charAt(i); // накапливает буквы которые стоят на нечетных местах
//            }
//        }
        String res = MyStrings.getEvenSymbols(str);
        System.out.println(res);
                
        // определить, является ли строка палиндромом
        
        // str = "Казак ";
        str = "Аргентина манит негра";
        
        //Алгоритм
        //первая буква совпадает с последней
        //вторая с предпоследнй
        // 0 = n - 1 - 0
        // 1 = n - 1 - 1
        // 2 = n - 1 - 2
        // 3 = n - 1 - 3
        // i = n - 1 - i, i = 0,1,2,3..., n / 2
        boolean state = MyStrings.isPalyndrom(str);
        
        
        if (state == true)
        {
            System.out.println("слово " +str+ " палиндром");
        }
        else
        {
            System.out.println("слово "+str+ " НЕ палиндром");
        }
    }
    
}
