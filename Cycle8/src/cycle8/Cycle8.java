
package cycle8;


public class Cycle8 {

   
    public static void main(String[] args) {
        double n = 6;
        double result = 0;
        
        for (double i = 1; i <= 2*n-1; i += 2)
        {
            result = result + i;
            System.out.println("Result: " + result);
        }
        
    }
    
}
