
package cycle6;


public class Cycle6 {

    
    public static void main(String[] args) {
        int a = 3;
        int b = 8;
        int result = 0;
        
        for (int i = a; i <= b ; i++)
        {
            result += (i * i);
        }
        System.out.println("Result: "+result);
    }
    
}
