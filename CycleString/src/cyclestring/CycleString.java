
package cyclestring;


public class CycleString {

   
    public static void main(String[] args) {
        //вывети цифры числа в обратно подрядке
        //324 --> 423
        // определить количество цифр в числе
        // определить, если ли в строке символ 7
        // посчитать количество пробелов
        int x = 324;
                     //0 1 2 идут под номерами
        String str1 = "324";
        //int y = Integer.parseInt(str1); // будет равно числу записанному символами(в строку)
        System.out.println("строка = "+str1);
        //jave, c# javasccript, c++
        // нумерация начинается от 0
        System.out.println("символ номер 2 в ней это "+str1.charAt(1));
        int n =str1.length(); // определяем количество символов в строке
        // номер последней букыв это количество символов - 1
        System.out.println("Перевернутая строка: ");
        for (int i = n - 1; i >= 0 ; i--)
        {
            System.out.print(str1.charAt(i));
        }
        //определяем входит ли число 7
        boolean state = false; // использует в качетсве Да/НЕТ , лучше использовать тип boolean
        char symbol = ' ';
        for (int i = 0; i < str1.length(); i++ )
        {
            if (str1.charAt(i) == symbol) // одинарные кавычки означают одну букву - сивол. Двойные кавчки означают строку.
            {
                System.out.println("символ 7 входит");
                state = true;
                break; //прерывает работу цикла
            }
        }
        if (state == false)
        {
            System.out.println("Символ 7 не входит.");
        }
        
    }
    
}
