
package myobjects;


public interface IEmployee {
    
    
    double calcSalary();
    String getTitle();
    double calcTax();
    
}
