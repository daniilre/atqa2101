
package myobjects;


public class Cleaner implements IEmployee {
    
    private double base;
    public Cleaner(double base)
    {
        this.base = base;
    }
    
    @Override
    public double calcSalary()
    {
        return this.base;
    }
    
    @Override
    public String getTitle()
    {
        return "cleaner";
    }

    @Override
    public double calcTax() {
        return this.calcSalary() * 0.065; // 6.5%
    }
    
    
}
