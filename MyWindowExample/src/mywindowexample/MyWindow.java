package mywindowexample;

import java.awt.FlowLayout;
import javax.swing.*;
//OOP Object Orient Programming
public class MyWindow extends JFrame { // наследуем класс JFrame. значит можем ниче вызывать this.setSize, this.setTitle,  this.setVisible
    //сообщения для пользователя традиционно Label это - тэги <p> <h1> <span>
    private JLabel lblMessage; // создал переменную, надпись
    //button - тэги input с type="button"
    private JButton btnAction; // создал переменную копку
    //JText - тэги input с type="txt"
    private JTextField txtInput;
    MyWindow()
    {
        System.out.println("call MyWindow()");
        this.setSize(400, 150);
        this.setTitle("Первое окно");
        lblMessage = new JLabel("Привет, мир!");
        btnAction = new JButton("Click me1");
        txtInput = new JTextField("Это  я Вася.");
        this.setLayout( new FlowLayout()); // элементы расположены друг за другом на форме
        
        this.add(lblMessage); // поместить сообщение на форму
        this.add(txtInput);
        this.add(btnAction); // поместить кнопку на форму

        this.setVisible(true);
        
    }
    
}
