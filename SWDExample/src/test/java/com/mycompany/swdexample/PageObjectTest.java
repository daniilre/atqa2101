/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.swdexample;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 *
 * @author EasternPeak
 */
public class PageObjectTest {
    
    public PageObjectTest() {
    }

    @org.junit.jupiter.api.BeforeAll
    public static void setUpClass() throws Exception {
    }
//
//    @org.junit.jupiter.api.AfterAll
//    public static void tearDownClass() throws Exception {
//    }
//
    @org.junit.jupiter.api.BeforeEach
    public void setUp() throws Exception {
    }

//    @org.junit.jupiter.api.AfterEach
//    public void tearDown() throws Exception {
//    }
    

    @org.junit.jupiter.api.Test
    public void testCanUseElementsInFirefox() {
        System.out.println("canUseElements");
        
        System.setProperty("webdriver.gecko.driver","C:\\WebDrivers\\geckodriver.exe"); // Setting system properties of FirefoxDriver
        WebDriver driver = new FirefoxDriver(); //Creating an object of FirefoxDriver
        PageObject mockServer = new PageObject(driver, "https://b2348e22-188a-4140-99d0-73626346be8c.mock.pstmn.io/myhome", "Тест 1");
        
//        PageObject instance = null;
        boolean expResult = true;
        boolean result = mockServer.canUseElements();
//        assertEquals(expResult, result);
        assertTrue(result == expResult);
    }
    
    @org.junit.jupiter.api.Test
    public void testCanUseElementsInChrome() {
        System.out.println("canUseElements");
        
        System.setProperty("webdriver.chrome.driver", "C:\\WebDrivers\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        PageObject mockServer = new PageObject(driver, "https://b2348e22-188a-4140-99d0-73626346be8c.mock.pstmn.io/myhome", "Тест 1");
        
//        PageObject instance = null;
        boolean expResult = true;
        boolean result = mockServer.canUseElements();
//        assertEquals(expResult, result);
        assertTrue(result == expResult);
    }


    
}
