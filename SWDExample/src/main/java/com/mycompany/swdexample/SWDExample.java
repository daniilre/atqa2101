
package com.mycompany.swdexample;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
//
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
//
import org.openqa.selenium.support.ui.WebDriverWait;
import static org.openqa.selenium.support.ui.ExpectedConditions.presenceOfElementLocated;
//работа с файлами
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

//

public class SWDExample {

    //
    //общий класс родитель все страниц
    //проверка 
    
    //
    public static void main(String[] args) {
         System.out.println("Hello from SWDExample");
       //
       System.setProperty("webdriver.gecko.driver","C:\\WebDrivers\\geckodriver.exe"); // Setting system properties of FirefoxDriver
       WebDriver driver = new FirefoxDriver(); //Creating an object of FirefoxDriver
       PageObject mockServer = new PageObject(driver, "https://b2348e22-188a-4140-99d0-73626346be8c.mock.pstmn.io/myhome", "Тест 1");
// //
// System.setProperty("webdriver.edge.driver", "C:\\WebDrivers\\msedgedriver.exe");
// WebDriver driver = new EdgeDriver();
//

        // System.setProperty("webdriver.chrome.driver", "C:\\WebDrivers\\chromedriver.exe");
        // WebDriver driver = new ChromeDriver();


//       driver.get("https://b2348e22-188a-4140-99d0-73626346be8c.mock.pstmn.io/myhome");
       System.out.println("Заголовок 1 "+mockServer.getTitle());
       System.out.println("Адрес 1 "+mockServer.getAddress());
       
       // что бы сделать скриншот
       if (mockServer.canUseElements())
       {
        mockServer.printScreenToFile();
       }
        
       //конец скриншота
       
       //https://devqa.io/selenium-css-selectors/
       // WebElement input = driver.findElement(By.name("idpass"));
       // WebElement input = driver.findElement(By.id("idpass"));
       WebElement input = driver.findElement(By.cssSelector(".myName"));
       input.sendKeys("My new value 123");
        
       System.out.println("Заголовок 2 "+driver.getTitle());
       System.out.println("Адрес 2 "+driver.getCurrentUrl());
       String inputText = input.getText();//компонент input
       System.out.println("text 1 "+inputText);
       String input1Content = input.getAttribute("value").toString();// test using text instead of value
       System.out.println("text 2 "+input1Content);
       
       
       driver.quit();
       //
    }
    
    public static void main1(String[] args) {
         System.out.println("Hello from SWDExample");
       //
       System.setProperty("webdriver.gecko.driver","C:\\WebDrivers\\geckodriver.exe"); // Setting system properties of FirefoxDriver
       WebDriver driver = new FirefoxDriver(); //Creating an object of FirefoxDriver
// //
// System.setProperty("webdriver.edge.driver", "C:\\WebDrivers\\msedgedriver.exe");
// WebDriver driver = new EdgeDriver();
//

        // System.setProperty("webdriver.chrome.driver", "C:\\WebDrivers\\chromedriver.exe");
        // WebDriver driver = new ChromeDriver();

       driver.manage().window().maximize();
       driver.manage().deleteAllCookies();
       driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
       driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
       driver.get("https://b2348e22-188a-4140-99d0-73626346be8c.mock.pstmn.io/myhome");
       System.out.println("Заголовок 1 "+driver.getTitle());
       System.out.println("Адрес 1 "+driver.getCurrentUrl());
       // что бы сделать скриншот
       File screenshort = ( (TakesScreenshot) driver ).getScreenshotAs(OutputType.FILE);
        // import java.util.Date;
        Date dateNow = new Date();
        SimpleDateFormat format = new SimpleDateFormat("hh_mm_ss");
        String fileName = format.format(dateNow)+".png";
        try {
            FileUtils.copyFile(screenshort, new File( "C:\\MyData\\"+fileName ) );
        } catch (IOException ex) {
            System.out.println("Что-то не так с именем файла");
        }
       //конец скриншота
       
       //https://devqa.io/selenium-css-selectors/
       // WebElement input = driver.findElement(By.name("idpass"));
       // WebElement input = driver.findElement(By.id("idpass"));
       WebElement input = driver.findElement(By.cssSelector(".myName"));
       input.sendKeys("My new value 123");
        
       System.out.println("Заголовок 2 "+driver.getTitle());
       System.out.println("Адрес 2 "+driver.getCurrentUrl());
       String inputText = input.getText();//компонент input
       System.out.println("text 1 "+inputText);
       String input1Content = input.getAttribute("value").toString();// test using text instead of value
       System.out.println("text 2 "+input1Content);
       
       
       driver.quit();
       //
    }
    
}
