
package com.mycompany.swdexample;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


public class LoginPage extends PageObject{
    
    
    private String address;
    private String title;
    private WebDriver driver;
    
    public LoginPage(WebDriver driver, String address, String title) {
        super(driver, address, title);
        this.driver = driver;
        this.title = title;
        this.driver = driver;
    }
    
    //вариант 1
    //тест = 
    //добавляем метод для тестирование 
    public boolean canLogin()
    {
        String expectedAddress = this.address;
        printScreenToFile();
        WebElement input = driver.findElement(By.cssSelector(".myName"));
        input.sendKeys("Ivanov");
        
//       System.out.println("Заголовок 2 "+driver.getTitle());
//       System.out.println("Адрес 2 "+driver.getCurrentUrl());
       String inputText = input.getText();//компонент input
       System.out.println("text 1 "+inputText);
       String input1Content = input.getAttribute("value").toString();// test using text instead of value
       System.out.println("text 2 "+input1Content);
       
       printScreenToFile();
        
        return true;
    }
    public boolean haveError()
    {
        return this.canUseElements() == false;
    }
    public boolean needRegistration()
    {
        return false;
    }
    //вариант 2
    // возвращаем объект ....
    //...
    //...
    //...
    public PageObject login()
    {
        PageObject login = new PageObject(driver, address, title);
        
        return login;
    }
    
    
}
