
package employeeexample;


public class Cleaner implements IEmployee{
    private double base;
    //Programmer (rate, hours)
    Cleaner(double salary)
    {
        this.base = salary;
    }

    @Override
    public double calcSalary() {
        return this.base;
    }

    @Override
    public String getTitle() {
        return "Cleaner";
    }
    
    
    
    
}
