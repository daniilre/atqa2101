
package junitsquaretest;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;


public class SquareTest {
    Square instance = null;
    public SquareTest() {
    }
    
//    @BeforeClass
    @org.junit.BeforeClass
    public static void setUpClass() {
        
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
//    @Before
    @org.junit.Before
    public void setUp() {
        this.instance = new Square(3);
    }
    
    @After
    public void tearDown() {
    }

//    @Test
    @org.junit.Test
    public void testCalcArea() {
        System.out.println("calcArea");
        double expResult = 9.0;
        double result = instance.calcArea();
//        assertEquals(expResult, result, 0.0);
        assertTrue(expResult == result);
//        fail("The test case is a prototype.");
    }

//    @Test
    @org.junit.Test
    public void testCalcP() {
        System.out.println("calcP");
        Square instance = new Square(3);
        double expResult = 12.0;
        double result = instance.calcP();
        assertTrue(expResult == result);
//        assertEquals(expResult, result, 0.0);
//        fail("The test case is a prototype.");
    }

//    @Test
//    @org.junit.Ignore
    @org.junit.Test
    public void testGetA() {
        testSetA(); // ?????? зачем это тут и почему без этого валится с ошибкой тест??
        System.out.println("getA");
//        Square instance = null; // т.к сверху завели общую переменную Square instance = null;
        double expResult = 0.0;
        double result = instance.getA();
//        assertEquals(expResult, result, 0.0);
        assertTrue(expResult == result);
//        fail("The test case is a prototype.");
    }

//    @Test
    @org.junit.Ignore
    public void testSetA() {
        System.out.println("setA");
        double a = 0.0;
//        Square instance = null; // т.к сверху завели общую переменную Square instance = null;
        instance.setA(a);
//        fail("The test case is a prototype.");
    }
    
}
