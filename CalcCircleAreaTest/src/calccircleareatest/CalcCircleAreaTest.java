
package calccircleareatest;

import java.util.Scanner;

public class CalcCircleAreaTest {

   
    public static void main(String[] args) {
        System.out.print("Enter the radius of a circle - ");
        double Radius = 0;
        Scanner myInput = new Scanner ( System.in );
        Radius = myInput.nextDouble();
        
        double Area = Math.PI * Radius * Radius;
        System.out.print("Area of a Circle is - " + Area);
        
        
//        double Radius = 13.25;
//        
//        double CircleArea = Math.PI * Radius * Radius;
//        
//        System.out.print("Area of a Circle is - " + CircleArea);
    }
    
}
