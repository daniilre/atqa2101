
package functionstest;

import java.util.Scanner;

public class FunctionsTest {

  
    public static void main(String[] args) {
        
        // задача написать функцию перевода Дюймов в сантиметры
        //прежде чем пишем код, делаем тест
        // в итоге будет модуль - класс, в котором полезные функции
        // и тесты к ним
        // этот класс выношу в отдельную библиотеку
        //потом переводить в см первые 10 дюймов
        
        double inch = 10;
        
//        Scanner input = new Scanner (System.in);
//        do
//        {
//            System.out.println("Введите дюймы ");
//            inch = input.nextDouble();
//        }while (inch < 0);
        
        for(int i = 1  ; i < 10  ; i++   )
        {
           //System.out.println("Номер шага "+i);
//           inch = i;
           double cm = MyConverter.toCm(i);
           System.out.print("дюймы - ");
           System.out.print(i);
           System.out.print(" cm - ");
           System.out.println(cm);
        }
        
        //test1(cm);
        
        
    }

    public static void test1(double value) {
        if (value == 25.4)
        {
            System.out.println(" test 1 - pass");
        
        }
        else
        {
            System.out.println("test 1 - fail");
        }
    }
    
}
