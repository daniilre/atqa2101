
package calcgeronareatest;

import java.util.Scanner;

public class CalcGeronAreaTest {

    
    public static void main(String[] args) {
        System.out.print("Enter the three sides of a triangle - ");
        double Side1 = 0;
        Scanner myInput = new Scanner ( System.in );
        Side1 = myInput.nextDouble();
//        String strX = myInput.nextLine(); // это перевод в строку, но с ней потом ничего не делаем дальше (можно обрабатывать ошибки например). Т.е. сейчас сработает и без этого.
        double Side2 = 0;
        Scanner myInput2 = new Scanner ( System.in );
        Side2 = myInput2.nextDouble();
//        String strX = myInput2.nextLine(); // ?
        double Side3 = 0;
        Scanner myInput3 = new Scanner ( System.in );
        Side3 = myInput3.nextDouble();
//        String strX = myInput3.nextLine();
        double SemiPerimeter = (Side1 + Side2 + Side3) / 2;
        double TriangleGeronArea = Math.sqrt(SemiPerimeter* (SemiPerimeter - Side1) * (SemiPerimeter - Side2) * (SemiPerimeter - Side3));
        
        System.out.print("Area of a Triangle(by Heron's Formula) is - " + TriangleGeronArea);
        
        
//        double Side1 = 3;
//        double Side2 = 4;
//        double Side3 = 5;
//        
//        double SemiPerimeter = (Side1 + Side2 + Side3) / 2;
//        double TriangleGeronArea = Math.sqrt(SemiPerimeter* (SemiPerimeter - Side1) * (SemiPerimeter - Side2) * (SemiPerimeter - Side3));
//        
//        System.out.print("Area of a Triangle(by Heron's Formula) is - " + TriangleGeronArea);
    }
    
}
