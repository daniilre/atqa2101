package cycletask1;

public class CycleTask1 {

    public static void main(String[] args) {

        System.out.println("Четные: ");
        int sum = 0;
        for (int i = 2; i <= 10; i = i + 2) {
            System.out.println(i);
            sum = sum + i;
        }
        System.out.println("Сума четных = " + sum);

        System.out.println("Произведение первых 7 целых чисел: ");
        int result = 1;
        for (int i = 1; i <= 7; i++) {
            System.out.print(i + "*");
            result = i * result;
        }
        System.out.println("");
        System.out.println("Произведение = " + result);

        //200-299 200=2+0+0=2 299=2+9+9=20
        task3();

    }

    public static void task3() {
        System.out.println("Sum of digits: ");
        int result = 0;
        for (int i = 200; i <= 299; i++) {
            int digitsSum = 0;
            String digits = Integer.toString(i);
            int n = digits.length(); // определяем количество символов в строке
            for (int j = 0; j < n; j++) {
                digitsSum += Integer.parseInt(Character.toString(digits.charAt(j)));
            }
            System.out.println("Sum (" + i + ") = " + digitsSum);

            result += digitsSum;
        }
        System.out.println("Total " + result);
    }

}
