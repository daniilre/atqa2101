
package unittest1;

import myobjects.*;

public class TestCase3 extends TestCase{
    IEmployee emp1;
    public TestCase3()
    {
        this.setUp();
    }
    protected void setUp()
    {
        //создаю сотрудника с известными входными данными
        //например такая комманда если уборщик IEmployee emp = new Cleaner(1000);
        this.emp1 = new Developer();
    }
    public String logResult()
    {
        if (this.tests())
        return "test_calTax_for_Developer_pass";
        else 
            return "test_calTax_for_Developer_fail";
    }
    protected boolean tests()
    {
        //double result = emp.calcSalary();
        double tax = this.emp1.calcTax();
        return tax == 65;
        
    }
    
}
