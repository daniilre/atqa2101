
package unittest1;

import myobjects.*;

public class TestCase2 extends TestCase{
    IEmployee emp1;
    public TestCase2()
    {
        this.setUp();
    }
    private void setUp()
    {
        //создаю сотрудника с известными входными данными
        //например такая комманда если уборщик IEmployee emp = new Cleaner(1000);
        this.emp1 = new Developer();
    }
    public String logResult()
    {
        if (this.tests())
            return "test_calcSalary_for_Developer_pass";
        else 
            return "test_calcSalary_for_Developer_fail";
    }
    private  boolean tests()
    {
        //double result = emp.calcSalary();
        double result = emp1.calcSalary();
        return result == 1000;
        
    }
    
}
