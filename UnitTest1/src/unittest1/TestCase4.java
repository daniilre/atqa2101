
package unittest1;

import myobjects.*;

public class TestCase4 extends TestCase{
    
    IEmployee emp1;
    public TestCase4()
    {
        this.setUp();
    }
    private void setUp()
    {
        //создаю сотрудника с известными входными данными
        //например такая комманда если уборщик IEmployee emp = new Cleaner(1000);
        this.emp1 = new Cleaner (1000);
    }
    public String logResult()
    {
        if (this.tests())
        return "test_calTax_for_Cleaner_pass";
        else 
            return "test_calTax_for_Cleaner_fail";
    }
    private  boolean tests()
    {
        //double result = emp.calcSalary();
        double tax = this.emp1.calcTax();
        return tax == 65;
        
    }
    
}
