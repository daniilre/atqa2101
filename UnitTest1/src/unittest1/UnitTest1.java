
package unittest1;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.FileReader;

import java.io.PrintWriter;

import java.io.IOException;// механизм обработки любой ошибки
import java.io.InputStreamReader;

public class UnitTest1 {


    public static void main(String[] args) {
        
        //1. Работаем с интерфейсами --  здес поддержка от программистов -- поддержка архитектуры
        //1.1 Объект тестирования должен быть внтри общего разделяемого модуля (иными словами библиотеки)
        
        //тесты на структуру: классов, моделей, протоколов
        //тесты на структуру требуют работы с механизмом рефлексии
        // и позволяют понять, что программисты внесли изменения в иерархию наследования
        // или переименовали методы
        
        //тесты на логику (test cases)
        //2. тесты должгы быть независимыми
        //т.е. test 1 = calcSalary() for Programmer
        // а test 2 = calcSalary() for Cleaner
        // test 3 = calcSalary() for Manager
        // т.е. их можно вызывать тесты независимо, в любом порядке
        //3. результат прогнозируемым - протоколируемым
        
//        System.out.println("test_calcSalary_for_Developer_pass");
//        System.out.println("test_calcSalary_for_Cleaner_pass");
//        System.out.println("test_calTax_for_Developer_pass");
//        System.out.println("test_calTax_for_Cleaner_pass");
//        //
//        System.out.println();
//        TestCase test1 = new TestCase1();
//        TestCase test2 = new TestCase2();
//        TestCase test3 = new TestCase3();
//        TestCase test4 = new TestCase4();
//        System.out.println(test2.logResult());
//        System.out.println(test1.logResult());
//        System.out.println(test3.logResult());
//        System.out.println(test4.logResult());
//        //Программирую каждый TestCase
//        //нужно перейти к циклам: это значит мне нужно ввести общего предка или ввести интерфейс
//        System.out.println();
        List<TestCase> tests = new ArrayList<>();
            tests.add(new TestCase2());
            tests.add(new TestCase1());
            tests.add(new TestCase3());
            tests.add(new TestCase4());
            tests.add(new TestCase5());
            tests.add(new TestCase6());
            PrintWriter output = null;
            try {
            output = new PrintWriter(new FileWriter("data.txt"));
            
            for(int i = 0; i < tests.size(); i++)
            {
                String strtmp = "test #"+i+" is "+tests.get(i).logResult();
                System.out.println(strtmp);
                output.println(strtmp);
            }
            output.close();
            //Нужно вносить изменения в алгоритм расчета зп developer
        } catch (IOException ex) {
           System.out.println("Не могу создать файл. Нет прав на запись.");
        }
          finally{
           output.close(); 
        }
        
        
    }
    
}
