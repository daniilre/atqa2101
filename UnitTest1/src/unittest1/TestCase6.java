
package unittest1;

import myobjects.*;

public class TestCase6 extends TestCase3{

    public TestCase6()
    {
        super();
        this.className = "MIddle Developer";
        this.methodName = "calcTax";
    }
    
    @Override
    protected boolean tests() {
        double tax = this.emp1.calcTax();
        return tax == (160 * 16.5) * 0.065;
        
    }

    @Override
    protected void setUp() {
        this.emp1 = new Developer(160, 16.5);
    }

//    @Override
//    public String logResult() {
//        if (this.tests())
//        return "test_calTax_for_MIddle_Developer_pass";
//        else 
//            return "test_calTax_for_Middle_Developer_fail";
//    }
    
    
    
}
